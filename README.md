
# Kayode Adegbite

Main README

[Exercise 1 - Development Installation](development_installation_step_1/README.md)

* Install AMPPS
* Install JDK
* Install Android Studio and create My First App
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
* Provide git command descriptions

[Exercise 2](exercise_2/README.md)

* Create a mobile app inside of Android Studio that has 2 interfaces and displays a recipe
* Screenshotofrunning application’s firstuser interface
* Screenshotofrunning application’s seconduser interface;

[Exercise 3](exercise_3/README.md)

* Create a mobile app inside Android Studio that has 2 interfaces and calculates the price of concert tickets
* Screenshot of ERD of petstore system;
* Screenshot of running application’s firstuser interface;
* Screenshot of running application’s seconduser interface;
* Links to the following files:
    * .a3.mwb
    * .a3.sql

[Exercise 4](a4/README.md)

* TBD