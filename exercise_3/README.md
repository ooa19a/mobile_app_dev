# Kayode Adegbite

## README file includes the following

> A mobile recipe app using Android Studio.

### Screenshots includes:

|screenshot | screenshot|
|------|--------|
|1. Screenshot of running application’s firstuser interface ![firstinterface screenshot](img/home.png) | 2. Screenshot of running application’s seconduser interface ![secondinterface screenshot](img/homepage.png) |
|3. Screenshot of Entity Relationship Diagram ![Screenshot of ERD](img/erd.png)  | 4. Link to the mwb file [erd link](docs/a3.mwb)  |