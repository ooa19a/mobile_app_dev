# Kayode Adegbite

## README file includes the following

> A mobile recipe app using Android Studio.

### Screenshots includes:

|screenshot | Description|
|------|--------|
|1. Screenshot of running application’s firstuser interface ![firstinterface screenshot](img/homepage_recipe.png) | 2. Screenshot of running application’s seconduser interface ![secondinterface screenshot](img/recipe_page.png) |

|3. Screenshot of activity_main.xml ![Screenshot of activity_main.xml](img/activitymain_screenshot.png)  | 4. Screenshot of activity_recipe ![Screenshot of activity_recipe n.xml](img/activity_recipe.png) |