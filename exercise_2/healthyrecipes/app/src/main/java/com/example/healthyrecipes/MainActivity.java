package com.example.healthyrecipes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setContentView() indicates necessary application resources location
        // "R" represent resource directory ("res")
        //"layout" refers to "Layout" directory, accessing activity_main.xml file
        setContentView(R.layout.activity_main);

        //instatiate Button object variable "button" that refers to btnRecipe
        Button button = (Button) findViewById(R.id.btnRecipe);
        //associate OnclickListener() to return button object variable
        //when button clicked starts recipe
        button.setOnClickListener(new View.OnClickListener() {
            @Override
                    public void onClick(View v){
                //you do not need to type in "packageContext" , Android does it for you
                startActivity(new Intent( MainActivity.this, Recipe.class));
            }

        });
    }
}